package menu;

import domain.User;
import repository.UserRepository;

import java.util.List;
import java.util.Scanner;

public class LoginMenu {

    private final Scanner scanner = new Scanner(System.in);
    private final UserRepository userRepository;
    private final MainMenu mainMenu;
    private final Registration registration;
    private User loggedInUser;

    public LoginMenu(UserRepository userRepository, MainMenu mainMenu, Registration registration){
        this.mainMenu = mainMenu;
        this.userRepository = userRepository;
        this.registration = registration;
    }

    public void displayLoginMenu () {
        System.out.println("***WELCOME TO E-SHOP***"+'\n'+"PLEASE CHOOSE OPTION BELOW: ");
        System.out.println("1-LOGIN"+'\n'+"2-REGISTRATION"+'\n'+"0-EXIT"+'\n');
        int choice = makeChoice();
        action(choice);

    }

    private int makeChoice() {
       int choice = scanner.nextInt();
        while(choice<0||choice>2){
            System.out.println("Wrong number!");
            choice = scanner.nextInt();
            scanner.nextLine();
        }
        return choice;
    }

    private void action (int num) {

        switch (num) {
            case 0:
                System.exit(0);
                break;

            case 1:
                login();
                break;

            case 2:
                registration();
                break;
        }
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    private void login () {
        System.out.println("PLEASE ENTER YOU USERNAME AND PASSWORD: or '0'- to exit");
        System.out.println("USERNAME: ");
        String username = scanner.next();
        if(username.equals("0")){
            System.exit(0);
        }
        System.out.println("PASSWORD: ");
        String password = scanner.next();
        if(password.equals("0")){
            System.exit(0);
        }
        List<User> allUsers = userRepository.getUsers();
        if(allUsers.stream().anyMatch(u-> u.getUsername().equals(username)&&u.getPassword().equals(password))){
            setLoggedInUser(allUsers.stream().filter(u->u.getUsername().equals(username)).findFirst().orElseThrow());
            mainMenu.displayMenu(loggedInUser);
        }
        else {
            System.out.println("WRONG USERNAME OR PASSWORD");
            login();
        }

    }



    private void registration () {

        this.registration.displayRegistrationMenu();

    }
}
