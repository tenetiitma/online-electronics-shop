package menu;

import domain.*;
import repository.CategoryRepository;
import repository.GroupRepository;
import repository.OrderRepository;
import repository.ProductRepository;

import java.time.LocalDate;
import java.util.*;

public class MainMenu {

    private final Scanner scanner = new Scanner(System.in);
    private final GroupRepository groupRepository;
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;
    private final Set<Product> basket = new TreeSet<>();
    private User loggedInUser;

    public MainMenu(GroupRepository groupRepository, CategoryRepository categoryRepository, ProductRepository productRepository, OrderRepository orderRepository) {
        this.groupRepository = groupRepository;
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
    }

    public void displayMenu (User user) {
        setLoggedInUser(user);
        System.out.println("***MAIN MENU***"+'\n'+"1-SELECT PRODUCT GROUP");
        System.out.println("2-CHECKOUT");
        System.out.println("3-MY ORDERS");
        System.out.println("0-EXIT");
        int choice = makeChoice();
        action(choice);
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    private int makeChoice() {
        int choice = scanner.nextInt();
        while(choice<0||choice>3){
            System.out.println("Wrong number!");
            choice = scanner.nextInt();
            scanner.nextLine();
        }
        return choice;
    }

    private void action (int num) {

        switch (num) {
            case 0:
                System.exit(0);
                break;

            case 1:
                displayGroups();
                break;

            case 2:
                checkout();
                break;

            case 3:
                displayOrders();
                break;
        }
    }

    private void displayOrders() {
        if(loggedInUser.getOrder().isEmpty()){
            System.out.println("YOU DON'T HAVE ANY ORDERS YET");
        }
        else {
            System.out.println("YOUR ORDERS: ");
            for (Order order : loggedInUser.getOrder()) {
                System.out.println(order);
            }
        }
       displayMenu(loggedInUser);
    }

    private void checkout () {

        if(basket.isEmpty()){
            System.out.println("YOUR BASKET IS EMPTY, PLEASE ADD PRODUCTS");
            displayMenu(loggedInUser);
        }
        else {
            placeOrder();
        }

    }

    private void placeOrder() {
        double total = 0.0;
        System.out.println("PRODUCTS IN BASKET: ");
        for(Product product : basket){
            System.out.println(product);
            System.out.println("TOTAL TO PAY: "+(total+=product.getPrice()));
        }
        System.out.println("DO YOU WANT TO PLACE ORDER? (Y/N)");
        scanner.nextLine();
        String res = scanner.nextLine();
        if(res.equalsIgnoreCase("y")){
            Order newOrder = new Order(total, LocalDate.now()
                    ,new Random().nextInt(1000)
                    ,"New Order",getLoggedInUser(),basket);
            orderRepository.addOrder(newOrder);
            for(Product product : basket)
            productRepository.updateProductOrder(product.getId(),newOrder);
            System.out.println("YOUR ORDER IS SAVED AND WILL BE PROCESSED SHORTLY");
            basket.clear();
            System.out.println("DO YOU WANT TO CONTINUE? (Y/N)");
            String response = "";
            response = scanner.nextLine();
            if(!response.equalsIgnoreCase("y")){
                System.exit(0);
            }
            else {
                displayMenu(loggedInUser);
            }
        }
        else {
            displayMenu(loggedInUser);
        }
    }

    private void displayGroups () {

        System.out.println("AVAILABLE GROUPS: ");
        List<Group> groups = groupRepository.getGroups();
        Collections.sort(groups);
        for(Group group : groups){
            System.out.println(group);
        }
        displayCategories(chooseGroup());

    }

    private void displayCategories (int num) {
        System.out.println("AVAILABLE CATEGORIES IN THIS GROUP: ");
        Group selectedGroup = groupRepository.getGroups().stream().filter(g->g.getId()==num).findFirst().orElseThrow();
        for(Category category : selectedGroup.getCategories()){
            System.out.println(category);
        }

        displayProducts(chooseCategory(num));
    }

    private void displayProducts(int catId) {
        System.out.println("AVAILABLE PRODUCTS IN THIS CATEGORY: ");
        Category selectedCategory = categoryRepository.getCategories().stream().filter(g->g.getId()==catId).findFirst().orElseThrow();
        for(Product product : selectedCategory.getProducts()){
            System.out.println(product);
        }
            System.out.println("ADD PRODUCT TO BASKET BY ID:");
            basket.add(productRepository.getProductById(chooseProduct(catId)));
            System.out.println("PRODUCT ADDED TO BASKET!");
            displayMenu(loggedInUser);

    }

    private int chooseProduct(int num) {
        int choice = scanner.nextInt();
        scanner.nextLine();
        while(choice<0||choice>categoryRepository.getCategoryById(num).getProducts().size()){
            System.out.println("Wrong number! Please select valid category ID");
            choice = scanner.nextInt();
            scanner.nextLine();
        }
        return choice;
    }

    private int chooseCategory (int num) {
        System.out.println("PLEASE ENTER CATEGORY NUMBER: ");
        int choice = scanner.nextInt();
        scanner.nextLine();
        while(choice<0||choice>groupRepository.getGroupById(num).getCategories().size()){
            System.out.println("Wrong number! Please select valid category ID");
            choice = scanner.nextInt();
            scanner.nextLine();
        }
        return choice;
    }

    private int chooseGroup () {
        System.out.println("PLEASE ENTER GROUP NUMBER: ");
        int choice = scanner.nextInt();
        scanner.nextLine();
        while(choice<0||choice>groupRepository.getGroups().size()){
            System.out.println("Wrong number! Please select valid group ID");
            choice = scanner.nextInt();
            scanner.nextLine();
        }
        return choice;
    }

}
