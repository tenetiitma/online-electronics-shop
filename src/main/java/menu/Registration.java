package menu;

import domain.User;
import repository.UserRepository;

import java.util.Scanner;

public class Registration {


    private final Scanner scanner = new Scanner(System.in);
    private final UserRepository userRepository;
    private final MainMenu mainMenu;

    public Registration(UserRepository userRepository, MainMenu mainMenu) {
        this.userRepository = userRepository;
        this.mainMenu = mainMenu;
    }

    public void displayRegistrationMenu () {

        System.out.println("REGISTRATION: "+'\n'+"PLEASE ENTER USERNAME AND PASSWORD: or '0' - to exit");
        System.out.println("USERNAME: ");
        String username = scanner.nextLine();
        if(username.equals("0")){
            System.exit(0);
        }
        System.out.println("PASSWORD: ");
        String password = scanner.nextLine();
        if(password.equals("0")){
            System.exit(0);
        }
        if(checkIfUserExists(username)){
            System.out.println("Username already exists, try again.");
            displayRegistrationMenu();
        }
        else {
            System.out.println("Please enter your email address: ");
            String email = scanner.nextLine();
            System.out.println("Please enter your Home address: ");
            String address = scanner.nextLine();
            mainMenu.displayMenu(registerNewUser(username,password,email,address));
        }
    }

    private boolean checkIfUserExists (String username) {
        return userRepository.getUsers().stream().anyMatch(u->u.getUsername().equals(username));
    }


    private User registerNewUser (String username, String password, String email, String address ) {
        User newUser  = new User(username,password,email,address);
        userRepository.saveUser(newUser);
        System.out.println("Registration successful!");
        newUser = userRepository.getUsers().stream().filter(u->u.getUsername().equals(username)).findFirst().orElseThrow();

        return newUser;
    }

}
