package domain;

import org.hibernate.annotations.SortNatural;

import javax.persistence.*;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "groupp")
public class Group implements Comparable<Group> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "groupp")
    @SortNatural
    private Set<Category> categories = new TreeSet<>();


    public Group(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Group() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    @Override
    public int compareTo(Group o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return
                "ID: " + id +
                ", NAME: '" + name + '\'' +
                '}';
    }


}
