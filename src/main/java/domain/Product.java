package domain;

import javax.persistence.*;

@Entity
@Table(name = "product")
public class Product implements Comparable<Product>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private double price;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @OneToOne
    @JoinColumn(name = "specification_id")
    private Specification specification;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    public Product(int id, String name, double price, String description, Category category, Specification specification, Order order) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.category = category;
        this.specification = specification;
        this.order = order;
    }

    public Product() {
    }


    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Specification getSpecification() {
        return specification;
    }

    public void setSpecification(Specification specification) {
        this.specification = specification;
    }


    @Override
    public int compareTo(Product o) {
        return this.name.compareTo(o.name);
    }


    @Override
    public String toString() {
        return
                "ID: " + id +
                ", NAME: " + name + '\'' +
                ", PRICE: " + price +
                ", DESCRIPTION: '" + description + '\'' +
                ", SPECIFICATION: " + specification +
                '}';
    }


}
