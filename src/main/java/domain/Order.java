package domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "amount")
    private double amount;

    @Column(name = "time_stamp")
    private LocalDate timeStamp;

    @Column(name = "confirmation_number")
    private int confirmationNumber;

    @Column(name = "desciption")
    private String description;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "order")
    private Set<Product> product;

    public Order(double amount, LocalDate timeStamp, int confirmationNumber, String description, User user, Set<Product> product) {
        this.amount = amount;
        this.timeStamp = timeStamp;
        this.confirmationNumber = confirmationNumber;
        this.description = description;
        this.user = user;
        this.product = product;
    }

    public Set<Product> getProduct() {
        return product;
    }

    public void setProduct(Set<Product> product) {
        this.product = product;
    }

    public Order() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDate getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDate timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getConfirmationNumber() {
        return confirmationNumber;
    }

    public void setConfirmationNumber(int confirmationNumber) {
        this.confirmationNumber = confirmationNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return
                "ID: " + id +
                ", AMOUNT " + amount +
                ", DATE: " + timeStamp +
                ", CONFIRMATION NR: " + confirmationNumber +
                ", PRODUCTS: '" + product + '\n'+'\'' +
                '}';
    }
}
