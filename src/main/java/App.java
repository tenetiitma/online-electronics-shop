
import menu.LoginMenu;
import menu.MainMenu;
import menu.Registration;
import repository.*;

public class App {

    public static void main(String[] args) {


        UserRepository userRepository = new UserRepository();
        GroupRepository groupRepository = new GroupRepository();
        CategoryRepository categoryRepository = new CategoryRepository();
        OrderRepository orderRepository = new OrderRepository();
        ProductRepository productRepository = new ProductRepository();


        MainMenu mainMenu = new MainMenu(groupRepository,categoryRepository,productRepository,orderRepository);
         Registration registration = new Registration(userRepository,mainMenu);

        LoginMenu loginMenu = new LoginMenu(userRepository,mainMenu, registration);



        loginMenu.displayLoginMenu();





    }
}
