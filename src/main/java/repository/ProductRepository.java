package repository;

import domain.Group;
import domain.Order;
import domain.Product;
import service.DBService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class ProductRepository {

    private final EntityManagerFactory entityManagerFactory = DBService.getEntityManagerFactory();


    public void saveProduct (Product product) {

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(product);
            em.getTransaction().commit();
            System.out.println("Product added!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public Product getProductById (int id) {

        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        Product product = em.find(Product.class, id);

        em.getTransaction().commit();

        return product;
    }


    public void updateProduct(Product product) {

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.merge(product);
            em.getTransaction().commit();
            System.out.println("Product updated!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public void updateProductOrder (int id, Order order) {

        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        Product product = em.find(Product.class,id);
        product.setOrder(order);

        em.getTransaction().commit();

    }

    public void deleteProduct(int id) {
        String sql = "DELETE Product WHERE id = :pid";
        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        int result = em.createQuery(sql).setParameter("pid", id).executeUpdate();

        em.getTransaction().commit();
        if (result > 0) {
            System.out.println("Product deleted!");
        }
    }

    public List<Product> getProducts () {

        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        return em.createQuery("from Product ", Product.class).getResultList();

    }

}
