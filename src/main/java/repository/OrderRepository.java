package repository;

import domain.Order;
import service.DBService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class OrderRepository {

    private final EntityManagerFactory entityManagerFactory = DBService.getEntityManagerFactory();

    public void addOrder(Order order) {

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(order);
            em.getTransaction().commit();
            System.out.println("Order added!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public void updateOrder(Order order) {

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.merge(order);
            em.getTransaction().commit();
            System.out.println("Order updated!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public void deleteOrder(int id) {
        String sql = "DELETE Order WHERE id = :oid";
        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        int result = em.createQuery(sql).setParameter("oid", id).executeUpdate();

        em.getTransaction().commit();
        if (result > 0) {
            System.out.println("Order deleted!");
        }
    }

    public List<Order> getOrders () {

        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        return em.createQuery("from Order ", Order.class).getResultList();

    }

}
