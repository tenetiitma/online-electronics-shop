package repository;

import domain.Specification;
import service.DBService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class SpecificationRepository {

    private final EntityManagerFactory entityManagerFactory = DBService.getEntityManagerFactory();

    public void saveSpecification(Specification specification) {

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(specification);
            em.getTransaction().commit();
            System.out.println("Specification added!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public void updateSpecification(Specification specification) {
        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.merge(specification);
            em.getTransaction().commit();
            System.out.println("Specification updated!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public void deleteSpecification(int id) {
        String sql = "DELETE Specification where id = :specId";

        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        int result = em.createQuery(sql).setParameter("specId", id).executeUpdate();

        em.getTransaction().commit();
        if (result > 0) {
            System.out.println("Specification deleted!");
        }
    }

    public List<Specification> getSpecifications () {

        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        return em.createQuery("from Specification ", Specification.class).getResultList();

    }

}
