package repository;

import domain.Group;
import service.DBService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class GroupRepository {

    private final EntityManagerFactory entityManagerFactory = DBService.getEntityManagerFactory();

    public void saveGroup(Group group) {

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(group);
            em.getTransaction().commit();
            System.out.println("Group added!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public Group getGroupById (int id) {

        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        Group group = em.find(Group.class, id);

        em.getTransaction().commit();

        return group;
    }

    public void updateGroup(Group group) {

        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.merge(group);
            em.getTransaction().commit();
            System.out.println("Group updated!");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public void deleteGroup(int id) {
        String sql = "DELETE Group WHERE id = :gid";
        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        int result = em.createQuery(sql).setParameter("gid", id).executeUpdate();

        em.getTransaction().commit();
        if (result > 0) {
            System.out.println("Group deleted!");
        }
    }

    public List<Group> getGroups () {

        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        return em.createQuery("from Group ", Group.class).getResultList();

    }

}
